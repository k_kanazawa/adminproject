const productDir = `/html/kanazawa/my-admin/`
const webpack = require('webpack')


module.exports = {
  /*
  ** Build configuration
  */
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        jQuery: 'jquery',
        $: 'jquery'
      })
    ],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

export default {
  mode: 'universal',
  srcDir: 'src/',
  router: {
    base: process.env.NODE_ENV === 'development' ? '/' : productDir
  },
  /*
  ** Customize the progress bar color
  */
  loading: {
    color: '#3B8070',
    height: '8px'
  },
  pageTransition: {
    name: 'fade-up',
    mode: 'out-in',
    beforeEnter(el) {
      console.log('Before enter...');
    }
  },
  layoutTransition: {
    name: 'fade-right',
    mode: 'out-in'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'my-admin',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js' },
      { src: './js/jquery.easing.js' },
      { src: './js/main.js' }
    ],
  },

  /*
  ** Nuxt.js modules
  */
 modules: [
   '@nuxtjs/style-resources'
  ],
  build: {
    postcss: {
      preset: {
        autoprefixer: {
          grid: true
        }
      }
    },
    extractCSS: true,
  },
  styleResources: {
    scss: [
      '@/assets/scss/modules/_import.scss',
    ]
  },
  css: [
    '@/assets/css/reset.css',
    '@/assets/css/font-awesome.min.css',
    '@/assets/css/transition/index.scss',
    '@/assets/scss/base.scss',
    '@/assets/scss/utility.scss',
  ]
}
