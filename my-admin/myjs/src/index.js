import '@babel/polyfill';
import { loader } from './init/commonFunc'
import { linkBox } from './plugin/linkBox'
import { tabMenu } from './plugin/tabMenu'
import { anchorLink } from './plugin/anchorLink'

import { buildMenu } from './navigation/buildMenu'





/* navigation
-------------------------------------------------------------------- */
buildMenu();

/* loader
-------------------------------------------------------------------- */
loader();

/* add Function
-------------------------------------------------------------------- */
linkBox();
tabMenu();
anchorLink();

