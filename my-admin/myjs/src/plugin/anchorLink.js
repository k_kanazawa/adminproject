import { headerHeight } from '../init/parameterName'
import { deviceWidth } from '../init/parameterName'
import { window_width } from '../init/commonFunc'
import { getParam } from '../init/commonFunc'


export const anchorLink = () => {
    $(window).on('load', function () {
        let settings = {
            heightSp: headerHeight.headerHeightSp,       // sp height
            heightTab: headerHeight.headerHeightTab,      // tab height
            heightPc: headerHeight.headerHeightPc,       // pc height
            heightPcFixed: headerHeight.headerHeightpcFixed,  // pc height is-fixed
            breakPoint: deviceWidth.pcWidth,              // break point
            $elmHeader: $('.l-header'),                           // header element
            moveY: 0,                                        // translateY animation
            notList: '.notAnchor,.colorBox,.modal',            // exclude Class
            fixSp: true,
            fixPc: true,
            fixTab: true,
            variableHeader: false,
            $elmAnchor: $('a[href*="#"]'),
            killFlg: $('body').is('.js-killSpace'),
            animation: 'easeInOutQuart'                          // default [linear or swing]
        };

        let heightSetting = function () {
            let newHeight = new Object;

            newHeight.Sp = (settings.fixSp) ? settings.heightSp : 0;
            newHeight.Tab = (settings.fixTab) ? settings.heightPc : 0;
            newHeight.Pc = (settings.fixPc) ? settings.heightPc : 0;
            newHeight.variable = function (e) {
                let headerHeight = settings.$elmHeader.outerHeight();
                let pos = e.offset().top;

                let variableHeight = (pos > headerHeight) ? settings.heightPcFixed : settings.heightPc;

                return variableHeight;
            }

            return newHeight;
        }

        let calculateHeader = function (e) {
            let height = 0;
            let h = (settings.variableHeader)
                ? (window_width() < settings.breakPoint) ? heightSetting().Sp : heightSetting().variable(e)
                : (window_width() < settings.breakPoint) ? heightSetting().Sp : heightSetting().Pc;

            if (settings.killFlg) {
                height = 0;
            } else {
                if (settings.moveY > 0) {
                    height = h - settings.moveY;
                } else {
                    height = h;
                }
            }

            return height;
        }

        let getLocationDirectoryName = function () {
            let dirArr = location.href.split("/");
            let dir = dirArr[dirArr.length - 2];

            return dir;
        }


        /**
         * ===================================================================================
         * = external links
         * ===================================================================================
         */
        if (localStorage.getItem('anchor') != -1) {
            let externalTargetElement = function () {
                let id = localStorage.getItem('anchor');
                let target = $('#' + id);

                return target;
            }

            if (externalTargetElement().length) {
                let obj = externalTargetElement();
                let pos = externalTargetElement().offset().top - calculateHeader(obj);
                $("html, body").animate({ scrollTop: pos }, 10);
            }
        }

        /* use parameter anchor */
        if (getParam('anchor')) {
            let obj = $('#' + getParam('anchor'));
            let pos = obj.offset().top - calculateHeader(obj);
            $("html, body").animate({ scrollTop: pos }, 10);
        }


        /**
         * ===================================================================================
         * = internal links
         * ===================================================================================
         */
        settings.$elmAnchor.not(settings.notList).on('click', function () {
            let href = $(this).attr('href');
            let idArr = href.split('#');

            let getIdName = function () {
                let id = idArr[idArr.length - 1];

                return id;
            }

            let locationDirectory = function () {
                let newDir = idArr[idArr.length - 2];

                return newDir;
            }

            let getHrefdirectoryName = function () {
                let linkArr = href.split('/');
                let linkDir = linkArr[linkArr.length - 2];

                return linkDir;
            }

            let internalTargetElement = function () {
                let target = $('#' + getIdName());

                return target;
            }


            if (getLocationDirectoryName() == getHrefdirectoryName() || getHrefdirectoryName() == undefined) {
                let obj = internalTargetElement();
                let pos = internalTargetElement().offset().top - calculateHeader(obj);
                $("html, body").animate({
                    scrollTop: pos
                }, 600, settings.animation);
                return false;
            } else {
                localStorage.setItem('anchor', getIdName());
                if (event.ctrlKey || event.metaKey) {
                    window.open(locationDirectory(), '_blank');
                    return false;
                } else {
                    location.href = locationDirectory();
                    return false;
                }
            }
        });


        /**
         * ===================================================================================
         * = initialize
         * ===================================================================================
         */
        localStorage.removeItem('anchor');

    });
}