export const tabMenu = () => {
    $('.js-tabMenu .js-tabMenu__link').on('click', function () {
        var index = $(this).index('.js-tabMenu .js-tabMenu__link');
        if ($(this).hasClass('is-active')) {
            return false;
        } else {
            $('.js-tabMenu__link').removeClass('is-active');
            $('.js-tabMenu__link').eq(index).addClass('is-active');
        }


        $.when(
            $('.js-tabContent').removeClass('is-active')
        ).done(function () {
            $('.js-tabContent').eq(index).addClass('is-active');
        });
    });
}