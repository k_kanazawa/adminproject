export const scrollin = () => {
    $('.js-scrollin').each(function () {
        var elemPos = $(this).offset().top;
        var scroll = $(window).scrollTop();
        if (scroll > elemPos - window_innerHeight) {
            $(this).addClass('is-scrollin');
        }
    });
}