export const window_width = () => {
    return window.innerWidth;
}

export const user_agent = () => {
    var ua = navigator.userAgent;
    if (ua.indexOf('iPhone') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {
        // スマートフォン用コード
        var uaFlg = 'sp';
    } else if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0) {
        // タブレット用コード
        var uaFlg = 'tab';

        if (ua.indexOf('iPad') > 0) {
            var uaFlg = 'iPad';
        }
    } else {
        // PC用コード
        var uaFlg = 'pc';
    }

    return uaFlg;
}

export const getParam = (name, url) => {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export const loader = () => {
    $(window).on('load', function () {

        // init
        $('.l-loader').delay(300).fadeOut(600);
    });
}
