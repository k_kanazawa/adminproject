export default class {
    constructor(obj, name) {
        this.obj = obj;
        this.name = name;
    }

    setClass() {
        this.obj.classList.add(this.name);
    }

    deliteClass() {
        this.obj.classList.remove(this.name);
    }

    headerFixed() {
        let headerHeight = $('.l-header').outerHeight();
        let scrollTop = $(window).scrollTop();

        let init = function () {
            if (scrollTop > headerHeight) {
                $('.l-header').addClass('is-fixed');
            } else {
                $('.l-header').removeClass('is-fixed');
            }
        }

        init();
        $(window).on('load.headerFixed scroll.headerFixed', function () {
            scrollTop = $(this).scrollTop();

            init();
        });
    }

}
