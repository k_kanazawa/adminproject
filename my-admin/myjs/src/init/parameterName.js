export const breakPoint = {
    ssWidth: 375,
    smWidth: 576,
    mdWidth: 768,
    lgWidth: 992,
    llWidth: 1025,
    xlWidth: 1200
}

export const deviceWidth = {
    spWidth     : breakPoint.mdWidth - 1,
    tabWidth_min: breakPoint.mdWidth,
    tabWidth_max: breakPoint.llWidth - 1,
    pcWidth     : breakPoint.llWidth
}

export const mediaQueryList = {
    bpSp     : window.matchMedia('(max-width: ' + deviceWidth.spWidth + 'px)'),
    bpTab    : window.matchMedia('(min-width: ' + deviceWidth.tabWidth_min + 'px) and (max-width: ' + deviceWidth.tabWidth_max + 'px)'),
    bpSpToTab: window.matchMedia('(max-width: ' + deviceWidth.tabWidth_max + 'px)'),
    bpPc     : window.matchMedia('(min-width: ' + deviceWidth.pcWidth + 'px)')
}

export const headerHeight = {
    headerHeightSp     : 60,
    headerHeightTab    : 60,
    headerHeightPc     : 0,
    headerHeightpcFixed: 80
}
