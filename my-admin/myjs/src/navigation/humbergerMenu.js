export const hunbergerMenu = () => {
    $(function () {
        // variables
        let scrollpos;

        function addFixed() {
            scrollpos = $(window).scrollTop();
            $('body').addClass('is-fix').css({
                'top': -scrollpos
            });
        }

        function removeFixed() {
            $('body').removeClass('is-fix').css({
                'top': 0
            });
            window.scrollTo(0, scrollpos);
        }

        function nav() {
            if (!$('body').is('.is-fix')) {
                $('.mark').addClass('is-active');
                addFixed();

                //Case: sNav indivisual action
                $('.l-header').on('transitionend webkitTransitionEnd oTransitionEnd mozTransitionEnd', function () {
                    $('.sNav').delay(100).queue(function () {
                        $(this).addClass('is-active').dequeue();
                    });
                    $(this).off('webkitTransitionEnd MozTransitionEnd mozTransitionEnd msTransitionEnd oTransitionEnd transitionEnd transitionend');
                });
            } else {
                $('.mark').removeClass('is-active');
                removeFixed();
                $('.sNav').removeClass('is-active');
            }
        }

        // internal links click
        $('.gNav a').on('click', function () {
            let flg = $(this).attr('href');

            let hrefArr = flg.split('/');
            let dirHref = hrefArr[hrefArr.length - 2];

            let locArr = location.pathname.split('/');
            let dirLoc = locArr[locArr.length - 2];
            if (flg.indexOf('#') != -1 && !$('body').hasClass('flgPc') && dirHref == dirLoc) {
                $('.mark').removeClass('is-active');
                removeFixed();
            }

        });

        // globalnav__bg click
        $('.globalNav__bg').on('click', function () {
            $('.mark').removeClass('is-active');
            removeFixed();
        });

        // build
        $('.l-mark').on('click', function () {
            nav();

            return false;
        });

    });
}