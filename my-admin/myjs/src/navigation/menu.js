/* mobileMenu
-------------------------------------------------------------------- */
export const mobileMenuBuild = () => {
    $('.js-subNavTrigger').on('click', function () {
        $(this).toggleClass('is-active');
        $(this).next().stop(true, false).slideToggle(300);
    });
}

export const mobileMenuDestroy = () => {
    $('.js-subNavTrigger').off('click');
    $('.js-subNavTrigger').next().removeAttr('style');
    $('.js-subNavTrigger').removeClass('is-active');
}


/* iOsMenu
-------------------------------------------------------------------- */
export const iOsMenuBuild = () => {
    $('.js-subNavTrigger').on('click', function () {
        $(this).toggleClass('is-active');
        if ($(this).next().css('display') == 'none') {
            $(this).parent().addClass('is-active');
            $(this).next().slideDown(300);
        } else {
            $(this).parent().removeClass('is-active');
            $(this).next().slideUp(300);
        }
    });
}


/* pcMenu
-------------------------------------------------------------------- */
export const pcMenuBuild = () => {
    $('.gNav__txt--item,.gNav__lnk--item,.sNav__txt--item,.sNav__lnk--item').parent().on('mouseenter', function () {
        $(this).addClass('is-active');
    }).on('mouseleave', function () {
        $(this).removeClass('is-active');
    });

    $('.js-subNavTrigger').parent('.sNav__item').on('mouseenter', function () {
        $(this).children('.subNav--child').slideDown(300);
    }).on('mouseleave', function () {
        $(this).children('.subNav--child').stop(true, false).slideUp(300);
    });

    $('.js-subNavTrigger').parent('.gNav__item').on('mouseenter', function () {
        $(this).children('.subNav').slideDown(300);
    }).on('mouseleave', function () {
        $(this).children('.subNav').stop(true, false).slideUp(300);
    });
}

export const pcMenuDestroy = () => {
    $('.gNav__txt--item,.gNav__lnk--item,.sNav__txt--item,.sNav__lnk--item').parent().off('mouseenter mouseleave');
    $('.js-subNavTrigger').parent('.sNav__item').off('mouseenter mouseleave');
    $('.js-subNavTrigger').parent('.gNav__item').off('mouseenter mouseleave');
    $('.js-subNavTrigger').next().removeAttr('style');
    $('.js-subNavTrigger').removeClass('is-active');
}


/* footerMobileMenu
-------------------------------------------------------------------- */
export const footerMobileMenuBuild = () => {
    $('.js-fNavTrigger').on('click', function () {
        $(this).next().stop(true, false).slideToggle();
        $(this).toggleClass('is-active');
    });
}

export const footerMobileMenuDestroy = () => {
    $('.js-fNavTrigger').off('click');
}


/* footerPCMenu
-------------------------------------------------------------------- */
export const footerPCMenuBuild = () => {
    $('.fNav__lnk--content,.fNav__lnk--child').on('mouseenter', function () {
        $(this).addClass('is-active');
    }).on('mouseleave', function () {
        $(this).removeClass('is-active');
    });
}

export const footerPCMenuDestroy = () => {
    $('.fNav__lnk--content,.fNav__lnk--child').off('mouseenter mouseleave');
    $('.fNav__lnk--content,.fNav__lnk--child').removeClass('is-active');
}