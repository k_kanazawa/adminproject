import { user_agent } from '../init/commonFunc'
import { mediaQueryList } from '../init/parameterName'
import classControll from '../init/classControll'
import * as menu from './menu'
import { hunbergerMenu } from './humbergerMenu'
import { sticky } from '../plugin/fixedsticky'


export const buildMenu = () => {
    hunbergerMenu();

    document.addEventListener('DOMContentLoaded', function () {
        let obj = document.body;
        let mqlSp = mediaQueryList.bpSp;
        let mqlTab = mediaQueryList.bpTab;
        let mqlSpToTab = mediaQueryList.bpSpToTab;
        let mqlPc = mediaQueryList.bpPc;
        let name;

        let checkBreakPoint = {
            Sp: function (mqlSp) {
                name = 'flgSp';
                const c = new classControll(obj, name);
                if (mqlSp.matches) {
                    c.setClass();
                    menu.footerMobileMenuBuild();
                } else {
                    c.deliteClass();
                    menu.footerMobileMenuDestroy();
                }
            },
            Tab: function (mqlTab) {
                name = 'flgTab';
                const c = new classControll(obj, name);
                if (mqlTab.matches) {
                    c.setClass();
                } else {
                    c.deliteClass();
                }
            },
            SpToTab: function (mqlSpToTab) {
                if (mqlSpToTab.matches) {
                    menu.mobileMenuBuild();
                } else {
                    menu.mobileMenuDestroy();
                }
            },
            Pc: function (mqlPc) {
                name = 'flgPc';
                const c = new classControll(obj, name);
                if (mqlPc.matches) {
                    sticky();
                    menu.pcMenuBuild();
                    menu.footerPCMenuBuild();
                    c.setClass();
                    c.headerFixed();
                } else {
                    menu.pcMenuDestroy();
                    menu.footerPCMenuDestroy();
                    c.deliteClass();
                    c.headerFixed();
                }
            },
            iOs: function () {
                name = 'flgiOs';
                const c = new classControll(obj, name);
                c.setClass();
            }
        }

        const commonFlgControl = function () {
            // breakPoint
            mqlSp.addListener(checkBreakPoint.Sp);
            mqlTab.addListener(checkBreakPoint.Tab);
            mqlSpToTab.addListener(checkBreakPoint.SpToTab);
            mqlPc.addListener(checkBreakPoint.Pc);

            // init
            checkBreakPoint.Sp(mqlSp);
            checkBreakPoint.Tab(mqlTab);
            checkBreakPoint.SpToTab(mqlSpToTab);
            checkBreakPoint.Pc(mqlPc);
        }

        if (user_agent() != 'iPad') {
            commonFlgControl();

        } else {
            commonFlgControl();

            // iPad
            checkBreakPoint.iOs();
            menu.iOsMenuBuild();
        }
    });

}
