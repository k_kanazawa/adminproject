const { VueLoaderPlugin } = require("vue-loader");
const path = require('path');

module.exports = {
  // モード値を production に設定すると最適化された状態で、
  // development に設定するとソースマップ有効でJSファイルが出力される
  mode: "production",

  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["vue-style-loader", "css-loader"]
      },
      {
        test: /\.vue$/,
        loader: "vue-loader"
      },

      {
        // 拡張子 .js の場合
        test: /\.js$/,
        use: [
          {
            // Babel を利用する
            loader: "babel-loader",
            // Babel のオプションを指定する
            options: {
              presets: [
                // プリセットを指定することで、ES2018 を ES5 に変換
                "@babel/preset-env"
              ]
            }
          }
        ]
      },

      {
        test: /\.json$/,
        loader: "json-loader",
        type: "javascript/auto"
      }
    ]
  },
  // import 文で .ts ファイルを解決するため
  resolve: {
    // Webpackで利用するときの設定
    alias: {
      vue$: "vue/dist/vue.esm.js"
    },
    extensions: ["*", ".js", ".vue", ".json"]
  },
  plugins: [
    // Vueを読み込めるようにするため
    new VueLoaderPlugin()
  ],

  // メインとなるJavaScriptファイル（エントリーポイント）
  entry: {
    "main": `./src/index.js`
  },

  externals: {
    jquery: "jQuery"
  },

  // ファイルの出力設定
  output: {
    //  出力ファイルのディレクトリ名
    // path: `${__dirname}/dist/js/`,
    path: path.resolve(__dirname, '../src/static/js'),
    // 出力ファイル名
    filename: "[name].js"
  },

  performance: {
    maxEntrypointSize: 400000,
    maxAssetSize: 400000
  }
};
